<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <script src="https://kit.fontawesome.com/8334dc67da.js" crossorigin="anonymous"></script>
    <title><?php the_title(); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header id="header">
        <div class="logo-content">
            <a class="header-content" href="<?php echo get_template_directory_uri(); ?>/../../../#home">
                <img id="logoHeader" class="logo-header" src="<?php echo get_template_directory_uri(); ?>/Logo.png" alt="">
            </a>
        </div>

            <?php
        wp_nav_menu( array( 'abalesbois' => 'header-menu',
                            'menu_class' => 'header-menu') ); 
            ?>
    </header>
    <?php wp_body_open(); ?>