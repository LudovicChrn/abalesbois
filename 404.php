<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <script src="https://kit.fontawesome.com/8334dc67da.js" crossorigin="anonymous"></script>
    <title><?php the_title(); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header id="header404">
        <div class="logo-content404">
            <a class="header-content404" href="<?php echo get_template_directory_uri(); ?>/../../../#home">
                <img id="logoHeader404" class="logo-header404" src="<?php echo get_template_directory_uri(); ?>/Logo.png" alt="">
            </a>
        </div>

            <?php
        wp_nav_menu( array( 'abalesbois' => 'header-menu',
                            'menu_class' => 'header-menu404') ); 
            ?>
    </header>

    <div class="page404">
        <h1>Oops !</h1>
        <img src="<?php echo get_template_directory_uri(); ?>/404WoodV2.png" alt="">
        <h2>Il semblerait que vous soyez arrivé sur une mauvaise page.</h2>
        <a class="goHome" href="<?php echo get_template_directory_uri(); ?>/../../../#home">
            Retourner à l'accueil
        </a>
    </div>

<?php get_footer(); ?>