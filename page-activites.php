

<?php get_header();?>
<h1 class="contact-title"><?php the_title(); ?></h1>
<div class="page_content"><?php the_field("description"); ?></div>
            <?php 
                // 1. On définit les arguments pour définir ce que l'on souhaite récupérer
                $args = array(
                    'post_type' => 'activite',
                );
                // 2. On exécute la WP Query
                $my_query = new WP_Query( $args );
                // 3. On lance la boucle !
                $i = 0;
                if( $my_query->have_posts() ) : while( $my_query->have_posts() ) : $my_query->the_post();
                    
                   ?>
                        <div class="bloc_entier">
                        <div class="bloc_titre"><h2><?php the_title();?></h2></div>
                        <div class="bloc2">
                        <div class="bloc_contenu"><?php the_field("description");?></div>
                        <div class="bloc_img"><?php the_post_thumbnail('post-thumbnail');?></div>
                        </div>
                        </div>
                   <?php
                $i++;
                endwhile;
                endif;
                wp_reset_postdata();
            ?>

<?php get_footer(); ?>