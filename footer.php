<?php wp_footer(); ?>
<footer>
    <div class="footer-group">

        <?php wp_nav_menu( array( 'abalesbois' => 'footer-menu' ) ); ?>

        <img class="logo-footer" src="<?php echo get_template_directory_uri(); ?>/Logo.png" alt="">

        <div class="footer-contact">
            <?php if ( is_active_sidebar( 'new-widget-area' ) ) : ?>
            <?php dynamic_sidebar( 'new-widget-area' ); ?>
            <?php endif; ?>
        </div>

    </div>
    <div class="footer-copyright">
        <p>&#169; 2021 - Tous droits réservés</p>
    </div>
</footer>
</body>
</html>