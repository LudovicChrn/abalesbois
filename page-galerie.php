<?php get_header(); ?>

    <h1 class="gallery-title"><?php the_title(); ?></h1>
    <?php the_content(); ?>

<?php get_footer(); ?>