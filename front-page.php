<?php get_header(); ?>

    <div id="home wrap" class="front-page">
        <section id="content">
          
            
            <?php the_content(); ?>

            <div class="touslescontenus">

                <!-- utilisation wp_query pour récupérer mon cpt dans page accueil -->

                <?php
                //on définit les arguments dans un tableau pour définir ce que l'on souhaite récupérer
                $args= array(
                    'post_type' => 'materiel',
                    'catergory_name' => 'outillage',
                    'posts_per_page' => 4,
                     
                );

                //on exécute la requête wp_query
                $my_query = new WP_Query($args);

                //on lance la boucle
                if($my_query->have_posts()):while($my_query->have_posts()):$my_query->the_post();
                ?>

             
                    <div class="contenu">
                        <div class="contenutexte">
                             <div class="title"> <h4> <?php the_title(); ?> </h4> </div>
                            <!-- <div class="title"> <?php the_content(); ?> </div> -->
                            <div class="title"> <?php the_field("description");?> </div>
                        </div>
                        <img src="<?php the_field('image')?>" alt="" height="200px" width="260px"/>
                    </div> 
              
               

                
                
                <?php

                endwhile;
                endif;

                //on réinitialise à la requête principale
                wp_reset_postdata();

                ?>

                <!-- fin requête wp_query-->
            </div>
        </section>

      <!-- <aside id="sidebar">
      <?php dynamic_sidebar('main-sidebar'); ?>
      </aside> -->
  </div>


<?php get_footer(); ?>