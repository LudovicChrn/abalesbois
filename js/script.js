window.onscroll = function() {myFunction()};

var header = document.getElementById("header");
var logo = document.getElementById("logoHeader");
var headerMenu = document.querySelector(".header-menu")
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
    header.style.flexDirection = "row";
    header.style.padding = "10px 0";
    headerMenu.style.padding = "0";
    logo.style.width = "40%";
  } else {
    header.classList.remove("sticky");
    header.style.flexDirection = "column";
    header.style.padding = "40px 0";
    headerMenu.style.padding = "40px 0 0 0";
    logo.style.width = "65%";
  }
}


