<?php

/* Charge le fichier style.css */

function load_scripts() {
    wp_enqueue_style('style', get_stylesheet_uri() ); 
}

add_action('wp_enqueue_scripts', 'load_scripts' );


/* Charge le fichier navigation.js */

function load_javascript(){
	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array(), '1.0.0', true );
}
add_action('wp_enqueue_scripts', 'load_javascript' );


/* Ajouter menu */

function abalesbois_register_menus()
{
	register_nav_menus(
		array(
		  'header-menu' => __( 'Header-Menu' ),
		  'footer-menu' => __( 'Footer-Menu' )
		)
	  );
}
add_action('init', 'abalesbois_register_menus');

/* Ajouter widget */

function register_custom_widget_area() {
	register_sidebar(
	array(
	'id' => 'new-widget-area',
	'name' => esc_html__( 'My new widget area', 'abalesbois' ),
	'description' => esc_html__( 'A new widget area made for testing purposes', 'abalesbois' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="widget-title-holder"><h3 class="widget-title">',
	'after_title' => '</h3></div>'
	)
	);
	}
	add_action( 'widgets_init', 'register_custom_widget_area' );

/* Ajouter des images sur les pages CPT */

function mon_theme_img_mise_en_avant() {
	add_theme_support( 'post-thumbnails' );
	add_image_size('post-thumbnail', 600, 400, true);
}

add_action('after_setup_theme', 'mon_theme_img_mise_en_avant' );

/* Supprimer des éléments de la page admin */

function wpdocs_remove_menus(){
	// remove_menu_page('tools.php');
	remove_menu_page('edit.php');
	remove_menu_page('mobile-menu-options');
}

add_action('admin_menu', 'wpdocs_remove_menus', 999);



/* (test) Ajout CPT */
// function wpm_custom_post_type(){
// 	$labels = array(
// 		'name' => 'Activités',
// 		'all_items' => 'Toutes les activités',
// 		'singular_name' => 'Activités',
// 		'add_new_item' => 'Ajouter une activités',
// 		'edit_item' => "Modifier l'activités"
// 	);

// 	$args = array(
//         'labels' => $labels,
//         'public' => true,
//         'show_in_rest' => true,
//         'has_archive' => true,
//         'supports' => array( 'title', 'editor','thumbnail' ),
//         'menu_position' => 5, 
//         'menu_icon' => 'dashicons-admin-customizer',
// 	);

// 	register_post_type( 'abalesbois', $args );
// }

// add_action( 'init', 'wpm_custom_post_type' );